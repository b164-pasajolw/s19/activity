console.log("Hello World!")

//ES6 Updates

// no.3 - Exponent Operator
const expoNum = 3 ** 3;
console.log(`The cube of 3 is ${expoNum}.`);

// no.4 - Template Literals

let base = 2;
console.log(`The cube of 2 is ${base * base * base}.`)

// no.5
const address = ["Purok 3", "Brgy. Lumbang", "Lipa City"];

// no.6
//Array Destructuring
const [street, barangay, city] = address;
console.log(`I live at ${street}, ${barangay}, ${city}.`)

// no.7
let animal = {
	type: "saltwater crocodile",
	weight: 1075,
	measurement: 20,
	excess: 3
};


// no.8 Object Destructuring

let {type, weight, measurement, excess} = animal

console.log(`Lolong was a ${type}. He weighed at ${weight} kgs with a measurement of ${measurement} ft ${excess} in.`);


//  no.9 Array of Numbers

let numbers = [1, 2, 3, 4, 5];

// no.10 Loop through the array using forEach, an arrow function and using the implicit return

numbers.forEach((number) => {
	console.log(`${number}`)
})

//alternative: arrayNumbers.forEach(num=>console.log(num));

// no.11

let reduceNumber = numbers.reduce((x, y) => x + y); 
console.log(reduceNumber)

/*
const sum = (x, y, a, b, c) => x + y + a + b +c;

let total = sum(1, 2, 3, 4, 5);
console.log(total);
*/
//correct answer : 
//let reduceNumber = numbers.reduce((x, y)) => x + y); 
//console.log(reduceNumber)

// no.12 

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
 // no.13
const thatDog = new Dog("Whinny", 2, "Weiner");
console.log(thatDog);
